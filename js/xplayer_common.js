/** @overwrite */
if (window.CoreComment) {
    CoreComment.prototype.init = function (recycle) {
        if (typeof recycle === 'undefined') { recycle = null; }
        if (recycle !== null) {
            this.dom = recycle.dom;
        } else {
            this.dom = document.createElement('div');
        }
        this.dom.className = this.parent.options.global.className;
        this.dom.innerHTML = this.text;

        // avoid xss
        var scripts = this.dom.getElementsByTagName('script');
        while (0 < scripts.length) {
            scripts[0].parentNode.removeChild(scripts[0]);
        }

        if (this._x !== undefined) {
            this.x = this._x;
        }
        if (this._y !== undefined) {
            this.y = this._y;
        }
        if (this._alpha !== 1 || this.parent.options.global.opacity < 1) {
            this.alpha = this._alpha;
        }
        if (this.motion.length > 0) {
            this.animate();
        }
    };
    (function(style, prop){
      if (!prop && !((prop = 'transform') in style)) { prop = ''; }
      if (!prop && !((prop = 'oTransform') in style)) { prop = ''; }
      if (!prop && !((prop = 'mozTransform') in style)) { prop = ''; }
      if (!prop && !((prop = 'webkitTransform') in style)) { prop = ''; }

      prop && Object.defineProperty(CoreComment.prototype, "x", {
          enumerable: true,
          configurable: true,
          get: function(){
              return this._x;
          },
          set: function(x){
              this.dom.style[prop] = 'translateX(' + (this._x = x) + 'px)';
          }
      });
    }(document.body.style));
}

window.requestAnimationFrame || (window.requestAnimationFrame = (
    window.mozRequestAnimationFrame
 || window.webkitRequestAnimationFrame
 || window.setTimeout
));
window.cancelAnimationFrame || (window.cancelAnimationFrame = (
    window.mozCancelAnimationFrame
 || window.webkitCancelAnimationFrame
 || window.clearTimeout
));

function skew_bullet(entry){
    //var sign = 1;// Math.random() > 0.5 ? -1 : 1;
    //return (sign * Math.min(25, Math.random() * 47 /* height */)).toFixed(0);
    return 0;
}

function life_bullet(entry){
    // duration according to content length
    var base = entry.original_content.length / (9 * 2);
    return 1000 * (5 + (
        8 * Math.max(base, Math.random())
    ) - (
        base > 3 ? base * Math.random() : 0
    ));
}

function html_bullet(entry){
  var avatar = entry.user.avatar;
  if (!avatar || -1 < avatar.indexOf('(null)')) {
      avatar = 'http://kibey-echo.b0.upaiyun.com/2014/11/1415951357.png';
  } else if (0 !== avatar.indexOf('http')) {
      avatar = 'http://kibey-sys-avatar.b0.upaiyun.com' + avatar;
  }
  var skew = skew_bullet(entry);
  return (
        '<div class="cmt-avatar" style="margin-top: ' + skew + 'px">'
          + '<a href="/user/info?user_id=' + entry.user.id + '">'
              + '<img alt="user-avatar" class="circle" src="' + avatar + '" />'
          + '</a>'
      + '</div>'
      + '<div class="cmt-content" style="margin-top: ' + skew + 'px">'
          + '<span class="cmt-nickname">' + entry.user.name + '</span>'
          + entry.original_content
      + '</div>'
  );
}

function html_player(mime, source, mobile){
    var type = mime && mime.toLowerCase().replace(/\/.+$/, '');
    if ('application' === type) {
        type = 'video';
    }
    if (!source || ('audio' !== type && 'video' !== type)) {
        return '';
    }
    if (mobile) {
        type = 'audio';
    }
    return (
        '<' + type + ' class="video-js" webkit-playsinline>'
        + '<source src="' + source + '" type="' + mime + '">'
      + '</' + type + '>'
    );
}

function flash_player(){
    return '/js/lib/dist/videojs-swf-4.6.1/video-js.mod.swf?v=506bef';
}

function sound_mine(url){
    var type = url.replace(/.+\.([a-z0-9]+)(?:[#?].*)?$/i, '$1').toLowerCase();
    return sound_mine.types[type] || 'video/mp4';
}
sound_mine.types = {
    'm3u8': 'application/x-mpegURL',
    'mp3':  'audio/mpeg',
    'mp4':  'video/mp4',
    'm4a':  'video/mp4'
};

function send_bullet(entry){
    if (!danmaku) { return; }

    danmaku.send({
        mode:  1,
        hash:  entry.id,
        date:  ~~entry.create_time,
        stime: (entry.start_time * 1000 || 300) + (1000 * Math.random()),
        text:  html_bullet(entry),
        dur:   life_bullet(entry)
    });
}

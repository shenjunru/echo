var danmaku, media;

function play_source(id, mime, source){
    var ctrl_panel, ctrl_loop, ctrl_danmaku;
    var bullet_get_href = 'http://api.app-echo.com/bullet/get';
    var bullet_get_data = { 'sound_id': id, 'time_part_no': 0 };
    var $mplayer = top.jQuery('#el_mplayer');
    var $splayer = jQuery('#el_splayer');
    var $comment = jQuery('#el_comment');
    var $btn_like = jQuery('a.like-btn');
    var vjs_controls = {
        'captionsButton':         false,
        'chaptersButton':         false,
        'fullscreenToggle':       false,
        'liveDisplay':            false,
        'muteToggle':             false,
        'playbackRateMenuButton': false,
        'remainingTimeDisplay':   false,
        'subtitlesButton':        false,
        'timeDivider':            false,
        'volumeControl':          false,
        'volumeMenuButton':       false,
        'progressControl': {
            'seekBar': {
                'seekHandle':     false
            }
        }
    };

    // dispose resources
    try { danmaku.stop(); danmaku.clear(); } catch(_) {} finally { danmaku = null; }
    try { media.pause(); media.dispose(); } catch(_) {} finally { media = null; }

    // initialize danmaku
    try {
        danmaku = new CommentManager(jQuery('#el_danmaku').get(0));
        danmaku.options.limit = danmaku.limiter = 12;
    } catch(_) {}
    danmaku._timer = 0;
    danmaku.startTimer = function(){
        if (0!=danmaku._timer) { return; }
        var a = (new Date).getTime();
        danmaku._timer = requestAnimationFrame(function(){
            danmaku._timer = requestAnimationFrame(arguments.callee, 10);
            var b = a; a = (new Date).getTime();
            danmaku.onTimerEvent(a - b, danmaku);
        }, 10);
    };
    danmaku.stopTimer = function(){
        cancelAnimationFrame(danmaku._timer);
        danmaku._timer = 0;
    };

    // initialize player
    $splayer.html(html_player(mime, source, false));
    var origin = media = videojs($splayer.find('.video-js').get(0), {
        'flash': {
            'swf': flash_player(),
            'flashVars': $splayer.find('.video-js').is('audio') ? {
                'mode': 'audio'
            } : {}
        },
        'width':  'auto',
        'height': 'auto',
        'controls': true,
        'children': {
            'bigPlayButton':     false,
            'errorDisplay':      false,
            'loadingSpinner':    false,
            'posterImage':       false,
            'textTrackDisplay':  false,
            'textTrackSettings': false,
            'controlBar':        false
        }
    }, function(){
        var data = top.mplayer && top.mplayer.current();
        var media = data && top.mplayer.media();

        media_ready(this);

        if (id == (data && data.sound_id)) {
            media_ready(media);
        } else {
            media = this;
        }

        init_danmaku(init_controls(media));
    });

    // integrate with mplayer
    if ($mplayer.size()) {
        $mplayer.off('.splayer')
        // use splayer core when play the current media
        .on('mplayer:videojs.splayer', function(_, $data, $media, reset){
            if (media && id == ($data && $data.sound_id)) {
                if (media !== $media) {
                    init_danmaku(init_controls(media_ready(media = $media)));
                }
                if (reset) {
                    media.currentTime(0);
                    media.paused() || media.pause();
                }
            } else if (media !== origin) {
                init_controls(media = origin);
            }
        })
        // sync loop mode with mplayer
        .on('mplayer:select.splayer', function(_, $data, $media){
            var mode = $splayer.data('mplayer.mode') || top.mplayer.mode();
            if (media === $media && !ctrl_loop.hasClass('disabled')) {
                top.mplayer.mode('loop-one');
            } else {
                top.mplayer.mode(mode);
            }
            $splayer.data('mplayer.mode', mode);
        })
        // sync like status with mplayer
        .on('mplayer:like.splayer', function(_, $data, state){
            if (id == ($data && $data.sound_id)) {
                $btn_like.toggleClass('active', state);
            }
        });
        $btn_like.removeAttr('onclick').off('click').on('click', function(){
            top.mplayer.like(id);
        });
        // add to the playlist when playing it
        origin.on(['play', 'playing'], function(){
            top.mplayer.append(id, true, false);
            setTimeout(function(){
            origin.pause();
            }, 0);
        });
    }

    function media_ready($media){
        $media.on('durationchange', function(event){
            ctrl_panel.getChild('durationDisplay').updateContent(event);
        });
        $media.on(['play', 'playing'], function(){
            ctrl_danmaku.hasClass('disabled') || danmaku && danmaku.start();
        });
        $media.on('pause', function(){
            ctrl_danmaku.hasClass('disabled') || danmaku && danmaku.stop();
        });
        $media.on('seeking', function(){
            ctrl_danmaku.hasClass('disabled') || danmaku && danmaku.stop();
        });
        $media.on('seeked', function(){
            ctrl_danmaku.hasClass('disabled') || danmaku && danmaku.start();
        });
        $media.on('timeupdate', function(){
            $comment.prop('placeholder', $comment.prop('placeholder').replace(
                /\d+(:\d+)+/, videojs.formatTime($media.currentTime(), $media.duration())
            ));
        });
        $media.on('ended', function(){
            if (!ctrl_loop.hasClass('disabled')) {
                $mplayer.size() || setTimeout(function(){
                    $media.currentTime(0);
                    $media.paused() && $media.play();
                }, 0);
            } else if (danmaku) {
                danmaku.stop();
                danmaku.clear();
            }
        });
        $media.on('dispose', function(){
            danmaku.stop();
            danmaku.clear();
            if ($media !== origin) {
                init_controls(media = origin);
            }
        });

        return $media;
    }

    function init_controls($media){
        try { ctrl_panel.dispose(); } catch(_) {}

        ctrl_panel = new videojs.ControlBar($media.player_, {
            children: vjs_controls
        });

        ctrl_loop = ctrl_panel.addChild('button');
        ctrl_loop.addClass('vjs-control vjs-loop-button disabled').el().inerHTML = (
            '<div class="vjs-control-content"><span class="vjs-control-text">Loop</span></div>'
        );
        ctrl_loop.on(['click', 'tap'], function(){
            if (ctrl_loop.hasClass('disabled')) {
                ctrl_loop.removeClass('disabled');
            } else {
                ctrl_loop.addClass('disabled');
            }
            // sync loop mode with mplayer
            if ($mplayer.size() && !$media.paused()) {
                top.mplayer.mode(ctrl_loop.hasClass('disabled') ? $splayer.data('mplayer.mode') : 'loop-one');
            }
        });

        ctrl_danmaku = ctrl_panel.addChild('button');
        ctrl_danmaku.addClass('vjs-control vjs-danmaku-button').el().inerHTML = (
            '<div class="vjs-control-content"><span class="vjs-control-text">Comments</span></div>'
        );
        danmaku && ctrl_danmaku.on(['click', 'tap'], function(){
            if (ctrl_danmaku.hasClass('disabled')) {
                ctrl_danmaku.removeClass('disabled');
                danmaku.start();
                danmaku.time($media.currentTime());
            } else {
                ctrl_danmaku.addClass('disabled');
                danmaku.stop();
                danmaku.clear();
            }
        });

        $splayer.append(ctrl_panel.el());

        // sync state with mplayer
        if (origin !== $media && !$media.paused()) {
            ctrl_panel.getChild('playToggle').onPlay();
        }

        return $media;
    }

    function init_danmaku($media){
        if (danmaku && !danmaku.filter) {
            danmaku.init();
            jQuery(window).on('resize.danmaku', function(){
                danmaku.setBounds();
            });
            jQuery.getJSON(bullet_get_href, bullet_get_data).then(function(json){
                if (1 === json.state && 'success' === json.message) {
                    // add to timeline
                    jQuery.each(json.result.data, function(_, entry){
                        danmaku.insert({
                            mode:  1,
                            hash:  entry.id,
                            date:  ~~entry.create_time,
                            stime: (entry.start_time * 1000 || 300) + (1000 * Math.random()),
                            text:  html_bullet(entry),
                            dur:   life_bullet(entry)
                        });
                    });
                    // next page
                    if (0 < json.result.next_step) {
                        bullet_get_data.time_part_no = json.result.next_step;
                        jQuery.getJSON(bullet_get_href, bullet_get_data).then(arguments.callee);
                    }
                }
            });
        }

        // use danmaku timer instead of player timeupdate event
        if (danmaku) {
            danmaku.onTimerEvent = function(){
                danmaku.constructor.prototype.onTimerEvent.apply(danmaku, arguments);
                ctrl_danmaku.hasClass('disabled') || danmaku.time($media.currentTime() * 1000);
            };
        }

        // sync state with mplayer
        if (origin !== $media && !$media.paused()) {
            ctrl_danmaku.hasClass('disabled') || danmaku.start();
        }

        return $media;
    }
}

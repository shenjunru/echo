;(function(videojs, store, mplayer){

    var list_state = jQuery.Deferred();
    var url_domain = location.protocol + '//' + location.host;
    var path_fetch = url_domain + '/sound/detail';
    var path_saved = url_domain + '/sound/my-sounds';
    var play_idx = ~(store && store.get('play_idx'));
    var list_sid = store && store.get('playlist') || [];
    var list_fav = [];
    var list_map = {};
    var list_limit = 100;
    var save_limit = 200;
    var fetch_size = 10;
    var load_idx_s = 0;
    var load_idx_e = 0;
    var load_retry = 0;
    var vjs_events = [
        'durationchange',
        'ended',
        'error',
        'firstplay',
        'loadedalldata',
        'loadeddata',
        'loadedmetadata',
        'loadstart',
        'pause',
        'play',
        'playing',
        'progress',
        'seeked',
        'seeking',
        'timeupdate',
        'volumechange',
        'waiting'
    ];
    var vjs_control = {
        'bigPlayButton':     false,
        'errorDisplay':      false,
        'loadingSpinner':    false,
        'posterImage':       false,
        'textTrackDisplay':  false,
        'textTrackSettings': false,
        'controlBar':        false
    };
    var vjs_volume = {
        children: {
            'volumeBar': {
                children: {
                    'volumeHandle': false
                }
            }
        }
    };
    var vjs_progress = {
        children: {
            'seekBar': {
                children: {
                    'seekHandle': false
                }
            }
        }
    };
    var jiathis = {
        ralateuid: {
            'tsina': 'echoapp'
        },
        appkey: {
            'tsina': '3898630147'
        },
        shortUrl: false,
        hideMore: true
    };
    var init_timer;
    var loader;
    var media;

    // mplayer container
    var $player = mplayer()
    .on('click', '.ctrl-toggle', function(){
        $player.removeClass('half');
        $player.toggleClass('full');
    })
    .on('mouseleave', function(){
        $player.removeClass('half');
    });

    // video.js container
    var $core = $player.find('> .core-panel');

    // play-ctrl
    var $info = jQuery('#el_mplayer_info')
    .on('click', '.media-cover:not(:empty)', function(){
        $player.addClass('full');
    })
    .on('mouseenter', '.media-cover:not(:empty)', function(){
        $player.addClass('half');
    });

    // play-ctrl
    var $ctrl = jQuery('#el_mplayer_ctrl')
    // base-ctrl
    .on('click', '.ctrl-play', function(){
        if (media) {
            if (media.paused()) {
                media.play();
            } else {
                media.pause();
            }
        } else {
            mplayer_play(list_map['s' + list_sid[play_idx]], false);
        }
    })
    .on('click', '.ctrl-prev', function(){
        mplayer_prev();
    })
    .on('click', '.ctrl-next', function(){
        mplayer_next();
    })
    .on('mouseleave', '.ctrl-loop ul', function(){
        jQuery(this).removeClass('chose');
    })
    .on('click', '.ctrl-loop li', function(){
        var $choices = jQuery(this).parent();
        if (!$choices.toggleClass('chose').hasClass('chose')) {
            $choices.append(this);
        }
    })
    .on('click', '.ctrl-list', function(){
        $list.toggleClass('enabled', jQuery(this).toggleClass('enabled').hasClass('enabled'));
    })
    // opts-ctrl
    .on('click', '.ctrl-like', function(){
        mplayer_like();
    })
    .on('click', '.ctrl-share', function(){
        mplayer_share();
    });

    // play-list
    var $list = jQuery('#el_mplayer_list')
    .on('click', '.ctrl-close', function(){
        $ctrl.find('.ctrl-list').add($list.get(0)).removeClass('enabled');
    })
    .on('click', '.ctrl-empty', function(){
        mplayer_empty();
    })
    .on('click', '.ctrl-remove', function(){
        mplayer_remove(jQuery(this).closest('li').attr('data-sid'));
    })
    .on('click', 'li', function(event){
        if (!jQuery(event.target).closest('.ctrl-remove').size()) {
            mplayer_play(list_map['s' + jQuery(this).attr('data-sid')], true);
        }
    });

    // mplayer closer
    $player.find('> .ctrl-close').on('click', function(){
        $list.find('.ctrl-close').click();
    });


    // enhance video.js
    videojs.setLocalStorage = function(key, value){
        store.set(key, value);
    };
    videojs.Player.prototype.muted = function(muted){
        if (muted !== undefined) {
            videojs.setLocalStorage('muted', muted);
            this.techCall('setMuted', muted);
            return this;
        }
        return this.techGet('muted') || false; // Default to false
    };

    // fake video.js components
    function FakeTech(player){
        this.isReady_ = true;
        this.player_ = player;
        this.muted = mplayer_muted;
        this.volume = mplayer_volume;
        this.setMuted = function(muted){
            this.player_.trigger('volumechange');
        };
        this.setVolume = function(percentAsDecimal){
            this.player_.trigger('volumechange');
        };
    }
    function FakePlayer(){
        this.cache_ = {};
        this.el_ = document.createElement('div');
        this.tech = new FakeTech(this);
    }
    FakeTech.prototype = videojs.Html5.prototype;
    FakePlayer.prototype = videojs.Player.prototype;
    var com_player = new FakePlayer();
    var com_mute = new videojs.MuteToggle(com_player);
    var com_volume = new videojs.VolumeControl(com_player, vjs_volume);
    var com_current, com_duration, com_progress;
    com_player.volume(mplayer_volume());
    com_player.muted(mplayer_muted());
    com_player.on('volumechange', function(event){
        $player.trigger('mplayer:' + event.type);
    });
    jQuery(com_mute.el()).find('>div').addClass('fa fa-fw fa-2x');
    $ctrl.find('.vols-ctrl').empty().append([ com_mute.el(), com_volume.el() ]);


    // APIs

    function mplayer_current(){
        return media && list_map['s' + list_sid[play_idx]];
    }
    function mplayer_media(){
        return media;
    }
    function mplayer_muted(){
        return null == store.get('muted') ? store.set('muted', false) : store.get('muted');
    }
    function mplayer_volume(){
        return null == store.get('volume') ? store.set('volume', 0.5) : store.get('volume');
    }
    function mplayer_append(sid, play, reset){
        if (-1 === jQuery.inArray(~~sid, list_sid)) {
            list_state.always(function(){
                var deferred = jQuery.Deferred();
                list_sid.push(~~sid);
                store.set('playlist', list_sid.slice(list_fav.length).slice(-save_limit));
                mplayer_fetch(deferred);
                deferred.then(function(){
                    mplayer_append(~~sid, play);
                });
            });
        } else if (play) {
            list_state.always(function(){
                mplayer_play(list_map['s' + sid], reset);
            });
        }
        return true;
    }
    function mplayer_remove(sid){
        var index, $item = $list.find('li[data-sid="' + sid + '"]').remove();
        if ($item.hasClass('playing')) {
            $info.html('<div class="media-cover"></div>');
            media_dispose();
        }

        if (load_idx_s && 0 <= (index = jQuery.inArray(~~sid, list_sid.slice(load_idx_e, load_idx_s)))) {
            load_idx_s -= 1;
        }

        if (-1 !== (index = jQuery.inArray(~~sid, list_sid))) {
            list_sid.splice(index, 1);

            if (-1 !== (index = jQuery.inArray(~~sid, list_fav))) {
                list_fav.splice(index, 1);
            }

            store.set('playlist', list_sid.slice(list_fav.length).slice(-save_limit));
        }

        delete list_map['s' + sid];
    }
    function mplayer_empty(){
        $list.find('.list-media').empty();
        $info.html('<div class="media-cover"></div>');
        media_dispose();
        store.set('playlist', []);
        load_idx_s = 0;
        load_idx_e = 0;
        play_idx = 0;
        list_map = {};
        list_sid = [];
    }
    function mplayer_pause(){
        media && media.pause();
    }
    function mplayer_play(data, reset){
        if (!data) { return; }
        if (!('s' + data.sound_id) in list_map) {
            mplayer_cache([data], [~~data.sound_id]);
        }

        if (data === mplayer_current()) {
            media.paused() && media.play();
            return;
        }

        var curr_idx = play_idx;
        if (-1 === (play_idx = jQuery.inArray(data.sound_id, list_sid))) {
            play_idx = curr_idx;
            return;
        }

        clearTimeout(init_timer);
        var $sound = $list.find('li[data-sid="' + data.sound_id + '"]');
        var $state = jQuery([ $sound.get(0), $player.get(0) ]);
        var _media = media_init(data, reset).ready(function(){
            init_timer = setTimeout(function(){
                _media.paused() && _media.play();
                if (_media.paused()) {
                    init_timer = setTimeout(arguments.callee, 10);
                }
            }, 10);

            // avoid bind again
            if (true === this.__mplayer__) { return; }
            this.__mplayer__ = true;

            // bind events
            this.on(['play', 'playing'], function(){
                $state.addClass('playing');
            });
            this.on('pause', function(){
                $state.removeClass('playing');
            });
            this.on('ended', function(){
                $state.removeClass('playing');
                setTimeout(function(){
                    var $mode = $ctrl.find('.ctrl-loop li:last-child');
                    switch (true) {
                        case $mode.hasClass('loop-all'):
                            mplayer_next();
                            break;

                        case $mode.hasClass('loop-one'):
                            setTimeout(function(){
                                media.currentTime(0);
                                media.paused() && media.play();
                            }, 0);
                            break;

                        case $mode.hasClass('loop-rnd'):
                            mplayer_next(true);
                            break;
                    }
                }, 0);
            });
            this.on(vjs_events, function(event){
                $player.trigger('mplayer:' + event.type, [data, this]);
            });
        });

        store.set('play_idx', play_idx);
        $sound.siblings().removeClass('playing');
        $state.addClass('playing');
        $info.html(html_info(data));
        $ctrl.find('.ctrl-like').toggleClass('active', data.is_like);
        $player.trigger('mplayer:select', [data, media]);
    }
    function mplayer_next(random){
        var index = play_idx - 1;
        if (null != random ? random : 'loop-rnd' === mplayer_mode()) {
            index = load_idx_e + Math.floor(Math.random() * (load_idx_s - load_idx_e));
        }
        if (index <= load_idx_e) {
            index = Math.max(0, load_idx_s - 1);
        }
        mplayer_play(list_map['s' + list_sid[index]], true);
    }
    function mplayer_prev(random){
        var index = play_idx + 1;
        if (null != random ? random : 'loop-rnd' === mplayer_mode()) {
            index = load_idx_e + Math.floor(Math.random() * (load_idx_s - load_idx_e));
        }
        if (index >= load_idx_s) {
            index = Math.max(0, load_idx_e - 1);
        }
        mplayer_play(list_map['s' + list_sid[index]], true);
    }
    function mplayer_mode(type){
        /* loop-off loop-one loop-all loop-rnd */
        var $choices = $ctrl.find('.ctrl-loop ul');
        if (null == type) {
            return $choices.find('>li:last-child').attr('class');
        } else {
            $choices.append($choices.find('>li.' + type).get(0));
        }
    }
    function mplayer_like(sid){
        var data = list_map['s' + (sid = sid || list_sid[play_idx])];
        data && jQuery.post(url_domain + '/sound/like?id='+data.sound_id, like_csrf, function(state){
            data.is_like = 1 === state;
            if (~~sid === list_sid[play_idx]) {
                $ctrl.find('.ctrl-like').toggleClass('active', data.is_like);
            }
            $player.trigger('mplayer:like', [data, data.is_like]);
        }, 'json');
    }
    function mplayer_share(sid, jiathis_config){
        var data = jiathis_config || list_map['s' + (sid || list_sid[play_idx])];
        if (!data) { return; }

        var $dialog = jQuery(html_share(data));
        var $jiathis = jQuery('div.jiathis_style');
        var old_config = window.jiathis_config;
        window.jiathis_config = jiathis_config || jQuery.extend({
            url: url_domain + '/sound/info?sound_id=' + data.sound_id,
            title: data.sound_name + ' （来自@echo回声APP）'
        }, jiathis);

        jQuery('html, body').css({ 'height':'100%', 'overflow':'hidden' });

        $dialog
        .on('click', 'ul a', function(){
            $jiathis.find(this.hash.replace(/^#?/, '.')).click();
            $player.trigger('mplayer:share', [data]);
            return false;
        })
        .one('click', '.close-btn', function(){
            $dialog.remove();
            jQuery('html, body').css({ 'height':'auto', 'overflow':'visible' });
            window.jiathis_config = old_config;
            return false;
        })
        .appendTo('body');
    }
    function mplayer_cache(items, part_1, part_2){
        var $results = $list.find('.list-media');
        items && part_1 && jQuery.each(part_1.reverse(), function(_, sid){
            jQuery.each(items, function(_, data){
                if (sid === ~~data.sound_id && !(('s' + data.sound_id) in list_map)) {
                    $results.prepend(html_item(list_map['s' + sid] = data));
                }
            });
        });
        items && part_2 && jQuery.each(part_2.reverse(), function(_, sid){
            jQuery.each(items, function(_, data){
                if (sid === ~~data.sound_id && !(('s' + data.sound_id) in list_map)) {
                    $results.append(html_item(list_map['s' + sid] = data));
                }
            });
        });
    }
    function mplayer_fetch(deferred){
        // retry failed 3 times
        if (loader || 3 === load_retry) {
            return true;
        }
        // over list limit
        if (load_idx_s === list_sid.length && (
            Math.min(list_limit, list_sid.length) <= (load_idx_s - load_idx_e)
        )) {
            return true;
        }

        // get load range
        var part_1 = list_sid.slice(load_idx_s, list_sid.length);
        var part_2 = list_sid.slice(Math.max(0, load_idx_e - fetch_size - part_1.length), load_idx_e);
        if (list_limit < (load_idx_s - load_idx_e + part_2.length)) {
            part_2.length -= (load_idx_s - load_idx_e - list_limit + part_2.length);
        }
        var data = part_1.concat(part_2);
        if (!data.length) {
            return true;
        }
        // load range data
        loader = jQuery.getJSON(path_fetch, { ids: '[' + data.join(',') + ']' }, function(json){
            // respond failed
            if (1 !== json.state && 'success' !== json.message) {
                load_retry += 1;
                return;
            }

            load_retry = 0;
            load_idx_s += part_1.length;
            load_idx_e -= part_2.length;

            mplayer_cache(jQuery.map(data, function(id){
                var sound = json.result[id];
                sound && !sound.user && (sound.user = {});
                return sound && {
                    author_id:    ~~sound.user.id,
                    author_name:  sound.user.name,
                    channel_id:   ~~sound.channel.id,
                    channel_name: sound.channel.name,
                    sound_id:     ~~sound.id,
                    sound_name:   sound.name,
                    sound_desc:   sound.info,
                    sound_mime:   sound_mine(sound.source),
                    sound_path:   sound.source,
                    sound_img:    sound.pic,
                    sound_size:   videojs.formatTime(~~sound.length),
                    is_like:      1 === sound.is_like
                }
            }), part_1, part_2);
        });
        loader.fail(function(){
            load_retry += 1;
        });
        loader.always(function(){
            loader = null;
            setTimeout(function(){
                if (mplayer_fetch(deferred)) {
                    list_state.resolve();
                    deferred && deferred.resolve();
                } else if (3 === load_retry) {
                    list_state.reject();
                    deferred && deferred.reject();
                }
            }, 0);
        });
    }
    function media_dispose(){
        if (media && jQuery.contains($core.get(0), media.el())) {
            try { media.pause(); media.dispose(); } catch(_) {} finally { media = null; }
        } else {
            try { media.pause(); } catch(_) {} finally { media = null; }
        }
        $ctrl.find('>.play-status').empty();
    }
    function media_init(data, reset){
        media_dispose();

        // create internal instance
        var player = html_player(data.sound_mime, data.sound_path, false);
        var $media = $core.html(player).find('.video-js');
        var _media = videojs($media.get(0), {
            'flash': {
                'swf': flash_player(),
                'flashVars': $media.is('audio') ? {
                    'mode': 'audio'
                } : {}
            },
            'width':  'auto',
            'height': 'auto',
            'preload':  false,
            'autoplay': false,
            'controls': false,
            'loop':     false,
            'children': vjs_control
        });

        // use internal or external instance
        var $event = new jQuery.Event('mplayer:videojs');
        $player.trigger($event, [data, _media, reset]);
        if (null != $event.result && _media !== $event.result) {
            try { _media.dispose(); } catch (_) {}
            _media = $event.result;
        }

        return media = _media.ready(media_raady);
    }
    function media_raady(){
        com_mute && com_mute.dispose();
        com_volume && com_volume.dispose();
        com_current && com_current.dispose();
        com_duration && com_duration.dispose();
        com_progress && com_progress.dispose();

        com_player = this.player_;
        com_mute = new videojs.MuteToggle(com_player);
        com_volume = new videojs.VolumeControl(com_player, vjs_volume);
        com_current = new videojs.CurrentTimeDisplay(com_player);
        com_duration = new videojs.DurationDisplay(com_player);
        com_progress = new videojs.ProgressControl(com_player, vjs_progress);

        jQuery(com_mute.el()).find('>div').addClass('fa fa-fw fa-2x');
        $ctrl.find('.vols-ctrl').empty().append([
            com_mute.el(),
            com_volume.el()
        ]);
        $ctrl.find('>.play-status').empty().append([
            com_current.el(),
            com_duration.el(),
            com_progress.el()
        ]);

        this.volume(mplayer_volume());
        this.muted(mplayer_muted());
    }
    function html_fill(html, data){
        return html.replace(/\$\{([^\s{}]+)\}/g, function(_, name){
            return data[name] || '&nbsp;';
        })
    }
    function html_item(data){
        return html_fill((
            '<li data-sid="${sound_id}" title="${sound_name}">'
                + '<i class="fa fa-play"></i>'
                + '<span class="media-name">${sound_name}</span>'
                + '<span class="media-author">${author_name}</span>'
                + '<span class="media-channel"><span>${channel_name}</span> 频道</span>'
                + '<span class="media-duration">${sound_size}</span>'
                + '<a class="ctrl-remove" href="javascript:void(0);" title="删除"><i class="fa fa-close"></i></a>'
          + '</li>'
        ), data);
    }
    function html_info(data){
        return html_fill((
              '<div class="media-cover"><img src="${sound_img}" alt=""/></div>'
            + '<div class="media-channel"><a href="' + url_domain + '/channel/info?id=${channel_id}" title="${channel_name}" target="mainFrame">${channel_name}</a> 频道</div>'
            + '<div class="media-name"><a href="' + url_domain + '/sound/${sound_id}" title="${sound_name}" target="mainFrame">${sound_name}</a></div>'
            + '<div class="media-author"><a href="' + url_domain + '/user/info?user_id=${author_id}" title="${author_name}" target="mainFrame">${author_name}</a></div>'
        ), data);
    }
    function html_share(data){
        return html_fill((
              '<div class="voice-share-inner-wp active">'
                + '<div class="voice-share-inner">'
                    + '<div class="voice-share">'
                        + '<div class="title-and-btn cf">'
                            + '<h4 class="fl">分享<a href="#">${channel_name}</a></h4>'
                            + '<span class="close-btn fr"></span>'
                        + '</div>'
                        + '<ul class="cf">'
                            + '<li><a href="javascript:jiathis_sendto(\'tsina\');">微博</a></li>'
                            + '<li><a href="javascript:jiathis_sendto(\'weixin\');">微信</a></li>'
                            + '<li><a href="javascript:jiathis_sendto(\'douban\');">豆瓣</a></li>'
                            + '<li><a href="javascript:jiathis_sendto(\'renren\');">人人</a></li>'
                            + '<li><a href="javascript:jiathis_sendto(\'qzone\');">QQ空间</a></li>'
                        + '</ul>'
                    + '</div>'
                + '</div>'
            + '</div>'
        ), data);
    }

    // expose api
    mplayer.current = mplayer_current;
    mplayer.append = mplayer_append;
    mplayer.remove = mplayer_remove;
    mplayer.empty = mplayer_empty;
    mplayer.pause = mplayer_pause;
    mplayer.play = mplayer_play;
    mplayer.next = mplayer_next;
    mplayer.prev = mplayer_prev;
    mplayer.mode = mplayer_mode;
    mplayer.media = mplayer_media;
    mplayer.like = mplayer_like;
    mplayer.share = mplayer_share;

    // load favourite list
    jQuery.getJSON(path_saved, {}, function(json){
        if (1 !== json.state && 'success' !== json.message) {
            return;
        }
        jQuery.each(json.result || [], function(_, sid){
            if (-1 === jQuery.inArray(~~sid, list_sid)) {
                list_sid.unshift(~~sid);
                list_fav.push(~~sid);
            }
        });

    // initialize player data
    }).always(function(){
        load_idx_s = load_idx_e = list_sid.length;
        if (!play_idx || (load_idx_s - list_limit) <= play_idx || play_idx >= load_idx_s) {
            play_idx = load_idx_s - 1;
        }
        if (mplayer_fetch()) {
            list_state.resolve();
        }
    });

    list_state.always(function(){
        var first = list_map['s' + list_sid[play_idx]];
        first && $info.html(html_info(first));
        first && $ctrl.find('.ctrl-like').toggleClass('active', first.is_like);
        $player.addClass('ready').trigger('mplayer:ready');
    });

})(window.videojs, window.store, mplayer);
function mplayer(){
    jQuery('#el_mplayer').size() || jQuery(document.body).append(
        '<div class="mplayer" id="el_mplayer">'
            + '<div class="panel core-panel"></div>'
            + '<div class="panel info-panel" id="el_mplayer_info">'
                + '<div class="media-cover"></div>'
            + '</div>'
            + '<div class="panel ctrl-panel" id="el_mplayer_ctrl">'
                + '<div class="base-ctrl">'
                    + '<a class="ctrl-prev" href="javascript:void(0);" title="上一首"><i class="fa fa-2x fa-step-backward"></i></a>'
                    + '<a class="ctrl-play fa-stack" href="javascript:void(0);" title="播放/暂停"><i class="fa fa-stack-1x fa-play"></i><i class="fa fa-stack-2x fa-circle-thin"></i></a>'
                    + '<a class="ctrl-next" href="javascript:void(0);" title="下一首"><i class="fa fa-2x fa-step-forward"></i></a>'
                    + '<a class="ctrl-loop" href="javascript:void(0);">'
                        + '<ul>'
                            + '<li class="loop-rnd" title="随机播放"><i class="fa fa-fw fa-2x fa-random"></i></li>'
                            + '<li class="loop-off" title="关闭循环"><div class="text">0</div><i class="fa fa-stack-2x fa-refresh"></i></li>'
                            + '<li class="loop-one" title="单曲循环"><div class="text">1</div><i class="fa fa-stack-2x fa-refresh"></i></li>'
                            + '<li class="loop-all" title="列表循环"><i class="fa fa-stack-2x fa-refresh"></i></li>'
                        + '</ul>'
                    + '</a>'
                    + '<a class="ctrl-list" href="javascript:void(0);" title="播放列表"><i class="fa fa-2x fa-align-left fa-flip-vertical"></i></a>'
                + '</div>'
                + '<div class="vols-ctrl">&nbsp;</div>'
                + '<div class="opts-ctrl">'
                    + '<a class="ctrl-like" href="javascript:void(0);" title="喜欢"><i class="fa fa-2x fa-heart"></i></a>'
                    + '<a class="ctrl-share" href="javascript:void(0);" title="分享"><i class="fa fa-2x fa-external-link"></i></a>'
                + '</div>'
                + '<div class="play-status"></div>'
            + '</div>'
            + '<div class="panel list-panel" id="el_mplayer_list">'
                + '<div class="panel-header">'
                    + '<span class="panel-title"><i class="fa fa-fw fa-align-left fa-flip-vertical"></i> 播放列表</span>'
                    + '<a class="ctrl-empty" href="javascript:void(0);" title="清空列表">清空列表</a>'
                    + '<a class="ctrl-close" href="javascript:void(0);" title="关闭列表"><i class="fa fa-2x fa-angle-down"></i></a>'
                + '</div>'
                + '<div class="panel-body">'
                    + '<ul class="list-media"></ul>'
                + '</div>'
            + '</div>'
            + '<a class="ctrl-toggle" href="javascript:void(0);" title="展开 / 收起">'
                + '<i class="fa fa-2x"></i>'
            + '</a>'
        + '</div>'
    );

    return jQuery('#el_mplayer');
}

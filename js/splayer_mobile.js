var danmaku, media;

function play_source(id, mime, source, flash){
    var bullet_get_href = 'http://api.app-echo.com/bullet/get';
    var bullet_get_data = { 'sound_id': id, 'time_part_no': 0 };
    var bullet_get_http = $http(bullet_get_href);

    // dispose resources
    try { danmaku.stop(); danmaku.clear(); } catch(_) {} finally { danmaku = null; }
    try { media.pause(); media.stop(); media.dispose(); } catch(_) {} finally { media = null; }

    // initialize danmaku
    danmaku = new CommentManager(el_danmaku);
    danmaku.options.limit = danmaku.limiter = 6;
    danmaku._timer = 0;
    danmaku.startTimer = function(){
        if (0!=danmaku._timer) { return; }
        var a = (new Date).getTime();
        danmaku._timer = requestAnimationFrame(function(){
            danmaku._timer = requestAnimationFrame(arguments.callee, 10);
            var b = a; a = (new Date).getTime();
            danmaku.onTimerEvent(a - b, danmaku);
        }, 10);
    };
    danmaku.stopTimer = function(){
        cancelAnimationFrame(danmaku._timer);
        danmaku._timer = 0;
    };

    // initialize player
    el_splayer.innerHTML = html_player(mime, source, true);
    var player = el_splayer.querySelector('.video-js');
    media = videojs(player, {
        'techOrder': [ 'html5' ],
        'width':  'auto',
        'height': 'auto',
        'controls': true,
        'children': {
            'button':            {},
            'playToggle':        {},
            'bigPlayButton':     false,
            'controlBar':        false,
            'errorDisplay':      false,
            'loadingSpinner':    false,
            'posterImage':       false,
            'textTrackDisplay':  false,
            'textTrackSettings': false
        }
    }, function(){
        var btn_danmaku = this.getChild('button');
        btn_danmaku.addClass('btn vjs-control vjs-danmaku-button').el().inerHTML = (
            '<div class="vjs-control-content"><span class="vjs-control-text">Comments</span></div>'
        );
        btn_danmaku.on(['click', 'tap'], function(){
            if (btn_danmaku.hasClass('disabled')) {
                btn_danmaku.removeClass('disabled');
                danmaku.start();
                danmaku.time(media.currentTime());
            } else {
                btn_danmaku.addClass('disabled');
                danmaku.stop();
                danmaku.clear();
            }
        });
        el_bullet_toggle.parentNode.replaceChild(btn_danmaku.el(), el_bullet_toggle);
        btn_danmaku.el().id = 'el_bullet_toggle';

        var btn_toggle = this.getChild('playToggle');
        btn_toggle.addClass('btn');
        el_play_toggle.parentNode.replaceChild(btn_toggle.el(), el_play_toggle);
        btn_toggle.el().id = 'el_play_toggle';

        this.on('play', function(){
            btn_danmaku.hasClass('disabled') || danmaku.start();
        });
        this.on('pause', function(){
            btn_danmaku.hasClass('disabled') || danmaku.stop();
        });
        this.on('ended', function(){
            danmaku.stop();
            danmaku.clear();
        });

        danmaku.init();
        window.addEventListener('resize', function(){
            danmaku.setBounds();
        }, false);
        // use danmaku timer instead of player timeupdate event
        danmaku.onTimerEvent = function(){
            danmaku.constructor.prototype.onTimerEvent.apply(danmaku, arguments);
            btn_danmaku.hasClass('disabled') || danmaku.time(media.currentTime() * 1000);
        };

        bullet_get_http.get(bullet_get_data, function(response){
            var json = JSON.parse(response);
            if (1 === json.state && 'success' === json.message) {
                // add to timeline
                json.result.data.forEach(function(entry){
                    danmaku.insert({
                        mode:  1,
                        hash:  entry.id,
                        date:  ~~entry.create_time,
                        stime: (entry.start_time * 1000 || 300) + (300 * Math.random()),
                        text:  html_bullet(entry),
                        dur:   life_bullet(entry)
                    });
                });
                // next page
                if (0 < json.result.next_step) {
                    bullet_get_data.time_part_no = json.result.next_step;
                    bullet_get_http.get(bullet_get_data, arguments.callee);
                }
            }
        });
    });
}

function $http(url){
    function ajax(method, url, args, callback, fallback){
        // Instantiates the XMLHttpRequest
        var client = new XMLHttpRequest();
        var uri = url;
        var body = null == args ? '' : args instanceof Object
            ? Object.keys(args).reduce(function(results, key){
                if (args.hasOwnProperty(key)) {
                    results.push(
                        encodeURIComponent(key) + '=' + encodeURIComponent(args[key])
                    );
                }
                return results;
            }, []).join('&')
            : args;

        if (body && method === 'GET') {
            uri += ('?' + body);
            body = '';
        }

        client.open(method, uri);
        client.send(body);

        client.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                callback && callback(this.response);
            } else {
                fallback && fallback(this.statusText);
            }
        };
        client.onerror = function () {
            fallback && fallback(this.statusText);
        };
    }

    return {
        'get' : function(args, callback, fallback) {
            ajax('GET', url, args, callback, fallback);
        },
        'post' : function(args, callback, fallback) {
            ajax('POST', url, args, callback, fallback);
        }
    };
}
